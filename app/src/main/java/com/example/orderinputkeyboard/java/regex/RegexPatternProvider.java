package com.example.orderinputkeyboard.java.regex;

public class RegexPatternProvider {
    public final String getInputPatternRegex = "^(\\d+\\*)*(([\\d\\w]+))+([+\\d\\w]*)";
    public final String checkSymbolErrorRegexPattern = "([++]{2,}+)|([**]{2,})";
    public final String getMemoValuesPattern="(\\+([\\d\\w]*))";
    public final String getQuantityValueRegex ="\\*.?";
}
