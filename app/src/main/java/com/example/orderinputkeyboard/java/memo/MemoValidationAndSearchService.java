package com.example.orderinputkeyboard.java.memo;

import android.content.Context;

import com.example.orderinputkeyboard.java.R;
import com.example.orderinputkeyboard.java.regex.RegexPatternProvider;
import com.example.orderinputkeyboard.java.service.MenuItemPluDataProviderService;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MemoValidationAndSearchService {
    public Set<String> processMemoValue(String memoValue, Context applicationContext) {
        RegexPatternProvider regexPatternProvider = new RegexPatternProvider();
        Pattern pattern = Pattern.compile(regexPatternProvider.getMemoValuesPattern);
        Matcher matcher = pattern.matcher(memoValue);
        Set<String> operations = new LinkedHashSet<>();
        while (matcher.find()) {
            String operation = matcher.group(2);
            if (!operation.equals(applicationContext.getString(R.string.Main_error_message))) {
                operations.add(operation);
            }
        }
        return operations;
    }

    public boolean checkMemoValidity(Set<String> memoList) {
        int count = 0;
        boolean setMemoValid = false;
        Map<String, String> memoValid = MenuItemPluDataProviderService.getAllMemos();
        if (memoList.size() >= 1) {
            for (String memo : memoList) {
                boolean isMemoAvailable = memoValid.containsKey(memo);
                if (isMemoAvailable) {
                    count++;
                    if (memoList.size() == count) {
                        setMemoValid = true;
                    }
                }
            }
            return setMemoValid;
        }
        return setMemoValid;
    }
}
