package com.example.orderinputkeyboard.java.regex;

import android.content.Context;

import com.example.orderinputkeyboard.java.R;
import com.example.orderinputkeyboard.java.memo.MemoValidationAndSearchService;
import com.example.orderinputkeyboard.java.plu.PluValidationAndSearchService;
import com.example.orderinputkeyboard.java.quantity.QuantityValidationService;

import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class InputValidationService {

    final RegexPatternProvider regexPatternProvider = new RegexPatternProvider();
    final String inputPatternRegex = regexPatternProvider.getInputPatternRegex;
    final PluValidationAndSearchService pluValidationAndSearchService = new PluValidationAndSearchService();

    final Pattern inputPattern = Pattern.compile(inputPatternRegex);

    public String validateInputWithRegex(String inputString, Context applicationContext) {

        String checkInputForMultipleConsecutiveSymbolPattern = regexPatternProvider.checkSymbolErrorRegexPattern;
        Pattern patternSymbolCheck = Pattern.compile(checkInputForMultipleConsecutiveSymbolPattern);
        Matcher matcher = inputPattern.matcher(inputString);
        Matcher matcherSymbols = patternSymbolCheck.matcher(inputString);
        if (!matcherSymbols.find()) {
            if (matcher.find()) {
                return getInputValueGroupsPluMemoQuantity(matcher, applicationContext);
            }
            return applicationContext.getString(R.string.Main_error_message);
        }
        return applicationContext.getString(R.string.Main_error_message);
    }

    private String getInputValueGroupsPluMemoQuantity(Matcher matcher, Context applicationContext) {
        boolean memoValid = true;
        QuantityValidationService quantityValidationService = new QuantityValidationService();
        MemoValidationAndSearchService memoValidationAndSearchService = new MemoValidationAndSearchService();
        String quantityGroup = matcher.group(1);
        String pluValue = matcher.group(2);
        String memoValue = matcher.group(4);
        Set<String> memoList = memoValidationAndSearchService.processMemoValue(memoValue, applicationContext);
        quantityValidationService.getQuantityFromInput(quantityGroup, applicationContext);
        if (memoList.size() != 0) {
            memoValid = memoValidationAndSearchService.checkMemoValidity(memoList);
        }
        if (pluValidationAndSearchService != null && memoValid) {
            return pluValidationAndSearchService.checkPluItem(pluValue, applicationContext);
        }
        return applicationContext.getString(R.string.Main_error_message);
    }

    public boolean isPluAvailableInInput(String inputString) {
        if (inputString != null) {
            Matcher matcher = inputPattern.matcher(inputString);
            return matcher.find();
        }
        return false;
    }

}