package com.example.orderinputkeyboard.java.quantity;

import android.content.Context;

import com.example.orderinputkeyboard.java.R;
import com.example.orderinputkeyboard.java.regex.RegexPatternProvider;

public class QuantityValidationService {

    public void getQuantityFromInput(String quantityGroup, Context context) {

        if (quantityGroup == null) {
            quantityGroup = context.getString(R.string.quantity_setToOne);
        }
        @SuppressWarnings("unused") String quantity = processQuantityValue(quantityGroup, context);
    }

    private String processQuantityValue(String quantity, Context context) {
        RegexPatternProvider regexPatternProvider = new RegexPatternProvider();
        if (quantity != null) {
            return quantity.replaceAll(regexPatternProvider.getQuantityValueRegex, context.getString(R.string.replace_doubleQuotes));
        }
        return null;
    }
}
