package com.example.orderinputkeyboard.java.service;

import android.content.Context;
import android.util.AttributeSet;

import com.flipboard.bottomsheet.BottomSheetLayout;

public class ExpendedBottomSheetLayout extends BottomSheetLayout {
    public ExpendedBottomSheetLayout(Context context) {
        super(context);
    }

    public ExpendedBottomSheetLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ExpendedBottomSheetLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ExpendedBottomSheetLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    public float getPeekSheetTranslation() {
        int contentHeight = getSheetView().getHeight();
        return contentHeight > 0 ? contentHeight : 500f;
    }

}