package com.example.orderinputkeyboard.java.plu;

import android.content.Context;

import com.example.orderinputkeyboard.java.R;
import com.example.orderinputkeyboard.java.service.MenuItemPluDataProviderService;

import java.util.Map;

public class PluValidationAndSearchService {
    public String checkPluItem(String pluValue, Context applicationContext) {
        Map<String, String> allPlus = MenuItemPluDataProviderService.getAllPlus();
        String itemNameOfPlu = allPlus.get(pluValue);
        if (itemNameOfPlu != null) {
            return itemNameOfPlu;

        } else {
            return applicationContext.getString(R.string.Main_error_message);
        }

    }
}
