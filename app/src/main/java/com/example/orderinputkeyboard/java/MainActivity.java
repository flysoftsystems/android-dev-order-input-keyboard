package com.example.orderinputkeyboard.java;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.orderinputkeyboard.java.regex.InputValidationService;
import com.example.orderinputkeyboard.java.service.ExpendedBottomSheetLayout;

import java.util.Objects;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Button buttonOk;
    Button buttonMemo;
    TextView textViewTitle;
    TextView textViewSubTitle;
    String inputString;
    final InputValidationService inputValidationService = new InputValidationService();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_main);

        disableOkButton();
        disableMemoButton();
        initiateNumericButton();
        getInputString();
    }


    private void initiateNumericButton() {


        Button buttonOne = findViewById(R.id.main_keyboard_one);
        Button buttonTwo = findViewById(R.id.main_keyboard_two);
        Button buttonThree = findViewById(R.id.main_keyboard_three);
        Button buttonFour = findViewById(R.id.main_keyboard_four);
        Button buttonFive = findViewById(R.id.main_keyboard_five);
        Button buttonSix = findViewById(R.id.main_keyboard_six);
        Button buttonSeven = findViewById(R.id.main_keyboard_seven);
        Button buttonEight = findViewById(R.id.main_keyboard_eight);
        Button buttonNine = findViewById(R.id.main_keyboard_nine);
        Button buttonZero = findViewById(R.id.main_keyboard_zero);
        buttonMemo = findViewById(R.id.main_keyboard_memo);
        buttonOk = findViewById(R.id.main_keyboard_ok);
        textViewTitle = findViewById(R.id.main_textView);
        textViewSubTitle = findViewById(R.id.main_subtitle);
        Button buttonAlphabets = findViewById(R.id.main_keyboard_alphabets);
        ImageButton buttonBackspace = findViewById(R.id.main_keyboard_backspace);
        Button buttonStar = findViewById(R.id.main_keyboard_star);

        buttonStar.setOnClickListener(this);
        buttonAlphabets.setOnClickListener(this);
        buttonOne.setOnClickListener(this);
        buttonTwo.setOnClickListener(this);
        buttonThree.setOnClickListener(this);
        buttonFour.setOnClickListener(this);
        buttonFive.setOnClickListener(this);
        buttonSix.setOnClickListener(this);
        buttonSeven.setOnClickListener(this);
        buttonEight.setOnClickListener(this);
        buttonNine.setOnClickListener(this);
        buttonZero.setOnClickListener(this);
        buttonMemo.setOnClickListener(this);
        buttonOk.setOnClickListener(this);

        buttonBackspace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String valueInput = textViewTitle.getText().toString();
                if (valueInput.length() != 0) {
                    textViewTitle.setText(valueInput.substring(0, valueInput.length() - 1));
                }
            }
        });
        buttonAlphabets.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                initiateAlphabeticButton();
            }
        });
    }

    private void initiateAlphabeticButton() {
        ExpendedBottomSheetLayout bottomSheet = findViewById(R.id.main_sheet);
        bottomSheet.setPeekOnDismiss(true);
        View sheetView = LayoutInflater.from(MainActivity.this).inflate(R.layout.alphabetic_keyboard, bottomSheet, false);
        Button buttonA = sheetView.findViewById(R.id.main_alphabet_keyboard_a);
        Button buttonB = sheetView.findViewById(R.id.main_alphabet_keyboard_b);
        Button buttonC = sheetView.findViewById(R.id.main_alphabet_keyboard_c);
        Button buttonD = sheetView.findViewById(R.id.main_alphabet_keyboard_d);
        Button buttonE = sheetView.findViewById(R.id.main_alphabet_keyboard_e);
        Button buttonF = sheetView.findViewById(R.id.main_alphabet_keyboard_f);
        Button buttonG = sheetView.findViewById(R.id.main_alphabet_keyboard_g);
        Button buttonH = sheetView.findViewById(R.id.main_alphabet_keyboard_h);
        Button buttonI = sheetView.findViewById(R.id.main_alphabet_keyboard_i);
        Button buttonJ = sheetView.findViewById(R.id.main_alphabet_keyboard_j);
        Button buttonK = sheetView.findViewById(R.id.main_alphabet_keyboard_k);
        Button buttonL = sheetView.findViewById(R.id.main_alphabet_keyboard_l);
        Button buttonM = sheetView.findViewById(R.id.main_alphabet_keyboard_m);
        Button buttonN = sheetView.findViewById(R.id.main_alphabet_keyboard_n);
        Button buttonO = sheetView.findViewById(R.id.main_alphabet_keyboard_o);
        Button buttonP = sheetView.findViewById(R.id.main_alphabet_keyboard_p);
        Button buttonQ = sheetView.findViewById(R.id.main_alphabet_keyboard_q);
        Button buttonR = sheetView.findViewById(R.id.main_alphabet_keyboard_r);
        Button buttonS = sheetView.findViewById(R.id.main_alphabet_keyboard_s);
        Button buttonT = sheetView.findViewById(R.id.main_alphabet_keyboard_t);
        Button buttonU = sheetView.findViewById(R.id.main_alphabet_keyboard_u);
        Button buttonV = sheetView.findViewById(R.id.main_alphabet_keyboard_v);
        Button buttonW = sheetView.findViewById(R.id.main_alphabet_keyboard_w);
        Button buttonX = sheetView.findViewById(R.id.main_alphabet_keyboard_x);
        Button buttonY = sheetView.findViewById(R.id.main_alphabet_keyboard_y);
        Button buttonZ = sheetView.findViewById(R.id.main_alphabet_keyboard_z);
        Button buttonCancel = sheetView.findViewById(R.id.main_alphabet_keyboard_cancel);

        final boolean pluAvailableInString = inputValidationService.isPluAvailableInInput(inputString);

        buttonA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_a));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_A));
                }
            }
        });


        buttonB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_b));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_B));
                }
            }
        });


        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_c));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_C));
                }
            }
        });

        buttonD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_d));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_D));
                }
            }
        });

        buttonE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_e));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_E));
                }
            }
        });


        buttonF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_f));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_F));
                }
            }
        });


        buttonG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_g));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_G));
                }
            }
        });


        buttonH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_h));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_H));
                }
            }
        });


        buttonI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_i));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_I));
                }
            }
        });


        buttonJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_j));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_J));
                }
            }
        });


        buttonK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_k));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_K));
                }
            }
        });


        buttonL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_l));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_L));
                }
            }
        });

        buttonM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_m));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_M));
                }
            }
        });


        buttonN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_n));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_N));
                }
            }
        });


        buttonO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_o));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_O));
                }
            }
        });


        buttonP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_p));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_P));
                }
            }
        });


        buttonQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_q));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_Q));
                }
            }
        });


        buttonR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_r));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_R));
                }
            }
        });


        buttonS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_s));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_S));
                }
            }
        });


        buttonT.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_t));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_T));
                }
            }
        });

        buttonU.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_u));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_U));
                }
            }
        });


        buttonV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_v));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_V));
                }
            }
        });

        buttonW.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_w));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_W));
                }
            }
        });

        buttonX.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_x));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_X));
                }
            }
        });

        buttonY.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_y));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_Y));
                }
            }
        });

        buttonZ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pluAvailableInString) {
                    textViewTitle.append(getString(R.string.keyboard_z));
                } else {
                    textViewTitle.append(getString(R.string.keyboard_Z));
                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textViewTitle.setText(getString(R.string.main_double_quotes));
            }
        });
        bottomSheet.showWithSheetView(sheetView);
    }

    private void getInputString() {

        textViewTitle.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                inputString = textViewTitle.getText().toString();
                processInputString(inputString, getApplicationContext());
            }
        });
    }

    private void processInputString(String inputString, Context applicationContext) {
        if (!textViewTitle.getText().toString().equals(null)) {
            InputValidationService inputValidationService = new InputValidationService();
            String plusSearchItem = inputValidationService.validateInputWithRegex(inputString, getApplicationContext());
            if (!plusSearchItem.equals(applicationContext.getString(R.string.Main_error_message))) {
                textViewSubTitle.setTextColor(Color.GRAY);
                textViewSubTitle.setText(plusSearchItem);
                enableMemoButton();
                enableOkButton();
            } else {
                displayErrorMessage(plusSearchItem);
            }
        } else {
            setSubtitleToDefault();
            disableMemoButton();
            disableOkButton();
        }

    }


    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.main_keyboard_one:
                textViewTitle.append("1");
                break;
            case R.id.main_keyboard_two:
                textViewTitle.append("2");
                break;
            case R.id.main_keyboard_three:
                textViewTitle.append("3");
                break;
            case R.id.main_keyboard_four:
                textViewTitle.append("4");
                break;
            case R.id.main_keyboard_five:
                textViewTitle.append("5");
                break;
            case R.id.main_keyboard_six:
                textViewTitle.append("6");
                break;
            case R.id.main_keyboard_seven:
                textViewTitle.append("7");
                break;
            case R.id.main_keyboard_eight:
                textViewTitle.append("8");
                break;
            case R.id.main_keyboard_nine:
                textViewTitle.append("9");
                break;
            case R.id.main_keyboard_zero:
                textViewTitle.append("0");
                break;
            case R.id.main_keyboard_star:
                textViewTitle.append("*");
                break;

            case R.id.main_keyboard_memo:
                textViewTitle.append("+");
                break;

            case R.id.main_keyboard_ok:
                textViewTitle.append("");
                break;

            default:
                break;
        }
    }


    private void disableOkButton() {
        buttonOk = findViewById(R.id.main_keyboard_ok);
        buttonOk.setEnabled(false);
        buttonOk.setBackgroundColor(Color.GRAY);
    }

    private void enableOkButton() {
        buttonOk = findViewById(R.id.main_keyboard_ok);
        buttonOk.setEnabled(true);
    }

    private void disableMemoButton() {
        buttonMemo = findViewById(R.id.main_keyboard_memo);
        buttonMemo.setEnabled(false);
    }

    private void enableMemoButton() {
        buttonMemo = findViewById(R.id.main_keyboard_memo);
        buttonMemo.setEnabled(true);
        buttonOk.setBackgroundColor(Color.rgb(0, 158, 51));
    }

    private void displayErrorMessage(String plusSearchItem) {
        disableOkButton();
        disableMemoButton();
        textViewSubTitle.setText(plusSearchItem);
        if (!textViewTitle.getText().toString().equals("")) {
            textViewSubTitle.setText(plusSearchItem);
            textViewSubTitle.setTextColor(Color.RED);
        } else {
            setSubtitleToDefault();
        }
    }

    private void setSubtitleToDefault() {
        textViewSubTitle.setText(R.string.main_double_quotes);
        textViewSubTitle.setHint(R.string.main_subTitle_hint);
    }

}

